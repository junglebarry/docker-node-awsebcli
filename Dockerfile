FROM node:latest

# Install python and pip
RUN apt-get -qq update && \
    apt-get install -y \
        python2.7-dev \
        python-pip \
    && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install the AWS Elastic Beanstalk CLI
RUN pip install awsebcli
